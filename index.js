const verifyKustomize = require('./lib/verifyConditions');
const prepareKustomize = require('./lib/prepare');
const publishKustomize = require('./lib/publish');

let verified = false;
let prepared = false;

async function verifyKustomize(pluginConfig, context) {
    await verifyKustomize(pluginConfig, context);
    verified = true;
}

async function prepare(pluginConfig, context) {
    if (!verified) {
        await verifyKustomize(pluginConfig, context);
    }

    await prepareKustomize(pluginConfig, context);
    prepared = true;
}

async function publish(pluginConfig, context) {
    if (!verified) {
        await verifyKustomize(pluginConfig, context);
    }
    if (!prepared) {
        await prepareKustomize(pluginConfig, context);
    }

    await publishKustomize(pluginConfig, context);
}

module.exports = {verifyKustomize, prepare, publish};
