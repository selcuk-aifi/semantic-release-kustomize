const fsPromises = require('fs').promises;
const path = require('path');
const yaml = require('js-yaml');
const semver = require('semver');

module.exports = async (pluginConfig, context) => {
    const logger = context.logger;

    let version;
    const appVersion = context.nextRelease.version;

    const filePath = path.join(pluginConfig.KustomizePath, 'kustomization.yaml');

    const KustomizeYaml = await fsPromises.readFile(filePath);
    const oldKustomize = yaml.load(KustomizeYaml);

    version = semver.inc(oldKustomize.version, context.nextRelease.type);

    let newKustomize;
    if (pluginConfig.onlyUpdateVersion) {
        newKustomize = yaml.dump({...oldKustomize, version: version});
        logger.log('Updating kustomization.yaml with version %s.', version);
    } else {
        newKustomize = yaml.dump({...oldKustomize, version: version, appVersion: appVersion});
        logger.log('Updating kustomization.yaml with version %s and appVersion %s.', version, appVersion);
    }

    await fsPromises.writeFile(filePath, newKustomize);
};
